const http = require('http');
const url = require('url');
const fs = require('fs');

if (!fs.existsSync('./file')) {
	fs.mkdirSync('./file');
}

fs.writeFileSync('./logs.json', `{"logs":[]}`);

module.exports = () => {
	const sendErr = (res, status, message) => {
		res.writeHead(status, { 'Content-Type': 'text/html' });
		res.end(message);
	}

	const sendContent = (type, content, res) => {
		res.statusCode = 200;
		res.setHeader('Content-Type', type);
		res.end(content);
	}

	const getFileValidator = (query) => {
		return query != null && query != '';
	}

	const postFileValidator = (filename, content) => {
		return filename != null && filename != '' && content != null;
	}

	const getFileHandler = (query, pathname, res) => {
		if (!getFileValidator(pathname)) {
			sendErr(res, 400, 'request is invalid');
			return;
		}
		fs.readFile(`.${pathname}`, (err, content) => {
			if (err) {
				sendErr(res, 400, `file with name ${pathname} not found`);
				return;
			}
			addLog(`file with name ${pathname} has been read`)
			sendContent('text/html', content, res);
		})
	}

	const postFileHandler = ({ filename, content }, pathname, res) => {
		if (!postFileValidator(filename, content)) {
			sendErr(res, 400, 'request is invalid');
			return;
		}
		fs.writeFile(`./file/${filename}`, content, (err) => {
			if (err) {
				sendErr(res, 400, `cant write file with name ${filename}`);
				return;
			}
			addLog(`file with name ${filename} has been saved`)

			res.statusCode = 200;
			res.end();
		})
	}

	const getLogsHandler = ({from, to}, pathname, res) => {
		fs.readFile(`./logs.json`, (err, content) => {
			if (err) {
				sendErr(res, 400, `logs file not found`);
				return;
			}
			if (!from && !to) {
				sendContent('application/json', content, res);
				return;
			}
			const result = JSON.parse(content).logs.filter(({ time }) => time >= from && time <= to);
			if (result.length < 1) {
				sendContent('text/html', `{"logs":[]}`, res);
				return;
			}
			sendContent('application/json', `{"logs":JSON.stringify(result)}`, res);
		})
	}

	const addLog = (logMessage) => {
		fs.readFile('./logs.json', (err, data) => {
			if (err) {
				return;
			}
			addLogMessage(data, logMessage);
		})
	}

	const addLogMessage = (data, logMessage) => {
		const logs = JSON.parse(data);
		logs.logs.push({
			time: Date.now(),
			message: logMessage
		});
		fs.writeFileSync('./logs.json', JSON.stringify(logs));
	}

	const getHandler = (method, pathname) => {
		if (method === 'POST') {
			return postFileHandler;
		}
		if (method === 'GET' && pathname === '/logs') {
			return getLogsHandler;
		}
		return getFileHandler;
	}

	http.createServer((req, res) => {
		const { query, pathname } = url.parse(req.url, true);
		const handler = getHandler(req.method, pathname);
		handler(query, pathname, res);
	}).listen(process.env.PORT || 8080);
}